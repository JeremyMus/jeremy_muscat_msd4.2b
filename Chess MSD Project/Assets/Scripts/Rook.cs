﻿using UnityEngine;
using System.Collections;

public class Rook : Chessmans {

    public override bool[,] PossibleMove()
    {
        bool[,] r = new bool[8, 8];
        Chessmans c;
        int i,j;

        //Right
        i = CurrentX;
        while (true) 
        {
            i++;
            if (i >= 8)
                break;

            c = Boardmanager.Instance.Chessmans[i, CurrentY];
            if (c == null)
                r[i, CurrentY] = true;
            else
            {
                if (c.isWhite != isWhite)
                    r[i, CurrentY] = true;

                break;
            }
        }
        //Left
        i = CurrentX;
        while (true) 
        {
            i--;
            if (i < 0)
                break;

            c = Boardmanager.Instance.Chessmans[i, CurrentY];
            if (c == null)
                r[i, CurrentY] = true;
            else
            {
                if (c.isWhite != isWhite)
                    r[i, CurrentY] = true;

                break;
            }
        }
        //Up
        i = CurrentY;
        while (true)
        {
            i++;
            if (i >= 8)
                break;

            c = Boardmanager.Instance.Chessmans[CurrentX, i];
            if (c == null)
                r[CurrentX, i] = true;
            else
            {
                if (c.isWhite != isWhite)
                    r[CurrentX, i] = true;

                break;
            }
        }
            //Down
            i = CurrentY;
            while (true)
            {
                i--;
                if (i < 0)
                    break;

                c = Boardmanager.Instance.Chessmans[CurrentX, i];
                if (c == null)
                    r[CurrentX, i] = true;
                else
                {
                    if (c.isWhite != isWhite)
                        r[CurrentX, i] = true;

                    break;
                }
            }
        return r;
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
