﻿using UnityEngine;
using System.Collections;

public class ExitGame : MonoBehaviour {
    public void OnMouseDown()
    {
        transform.localScale = new Vector3(1.3f, 1.3f, 1f);
        StartCoroutine(SceneChange());

    }

    private IEnumerator SceneChange()
    {
        yield return new WaitForSeconds(0.5f);
        Application.Quit();
    }

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
