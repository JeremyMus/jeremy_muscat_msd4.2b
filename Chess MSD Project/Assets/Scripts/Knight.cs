﻿using UnityEngine;
using System.Collections;

public class Knight : Chessmans {

    public override bool [,] PossibleMove ()
    {
        bool[,] r = new bool[8, 8];

        

        //Up to right
        KnightMove(CurrentX + 1, CurrentY + 2, ref r);

        //Right to Up
        KnightMove(CurrentX + 2, CurrentY + 1, ref r);

        //Right to Down
        KnightMove(CurrentX + 2, CurrentY - 1, ref r);

        //Up to left
        KnightMove(CurrentX - 1, CurrentY + 2, ref r);

        //Down to left
        KnightMove(CurrentX - 1, CurrentY - 2, ref r);

        //Down to right
        KnightMove(CurrentX + 1, CurrentY - 2, ref r);

        KnightMove(CurrentX + 1, CurrentY + 2, ref r);

        //Left to up
        KnightMove(CurrentX - 2, CurrentY + 1, ref r);

        //Left to down
        KnightMove(CurrentX - 2, CurrentY - 1, ref r);



        return r;
    }

    public void KnightMove(int x,int y,ref bool [,]r)
    {
        Chessmans c;
        if (x >= 0 && x < 8 && y >= 0 && y < 8)
        {
            c = Boardmanager.Instance.Chessmans[x, y];
            if (c == null)
                r[x, y] = true;

            else if (isWhite != c.isWhite)
                r[x, y] = true;
        }
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
