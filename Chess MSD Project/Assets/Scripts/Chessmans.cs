﻿using UnityEngine;
using System.Collections;

public abstract class Chessmans : MonoBehaviour {

    public int CurrentX { set; get;}
    public int CurrentY { set; get;}
    public bool isWhite;

    public void SetPosition(int x,int y)
    {
        CurrentX = x;
        CurrentY = y;
    }

    public virtual bool[,] PossibleMove ()
    {
        return new bool[8,8];
    }
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
