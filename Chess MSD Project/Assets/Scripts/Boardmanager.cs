﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.SceneManagement;

public class Boardmanager : MonoBehaviour {

    public static Boardmanager Instance { set; get; }
    private bool[,]AllowedMoves { set; get; }
   
    public Chessmans [,] Chessmans { set; get; }
    private Chessmans selectedChessman;

    public AudioClip chessmove;
    public AudioClip gamefinish;

    public TextMesh turnindicator;
    
    private const float Tile_Size = 1.0f;
    private const float Tile_Offset = 0.5f;

    private int selectionX = -1;
    private int selectionY = -1;

    public GameObject exit;
    public GameObject resume;

    public List<GameObject> chesspieceprefabs;
    private List<GameObject> activeChesspiece;

    public bool isWhiteTurn = true;
    
	// Use this for initialization
	void Start () {

        
        exit.SetActive(false);
        resume.SetActive(false);
        Scene currentscene = SceneManager.GetActiveScene();
        string scenename = currentscene.name;

        if (scenename == "NewGame")
        {
            SpawnAllChesspieces();
            Instance = this;
        }

        else if (scenename == "RandomGame")
        {
            SpawnRandomChesspieces();
            Instance = this;
        }

	
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown("escape"))
        {
            exit.SetActive(true);
            resume.SetActive(true);

            
        }

        
        DrawChessboard();
        UpdateSelection();


        if(Input.GetMouseButtonDown(0))
        {
            if (selectionX >= 0 && selectionY >= 0)
                {
                if (selectedChessman == null)
                {
                    //select
                    SelectChesspiece(selectionX, selectionY);
                }
                else
                {
                    //move
                    MoveChesspiece(selectionX, selectionY);
                }
            }
        }
	}

    private void SelectChesspiece(int x,int y)
    {
        
        if (Chessmans[x, y] == null)
            return;
        if (Chessmans[x, y].isWhite != isWhiteTurn)
            return;
        AllowedMoves = Chessmans[x, y].PossibleMove();
        selectedChessman = Chessmans[x, y];
        Highlights.Instance.HighlightAllowedMoves(AllowedMoves);
    }

    
    private void MoveChesspiece(int x,int y)
    {

        if (AllowedMoves[x,y])
        {

            Chessmans c = Chessmans[x, y];
            if (c != null && c.isWhite != isWhiteTurn)
            {
                //When a piece is captured

                //If piece is King

                if (c.GetType() == typeof(King))
                {
                    //Game Ended
                    EndGame();
                    return;
                }
                activeChesspiece.Remove(c.gameObject);
                Destroy(c.gameObject);
            }
            AudioSource.PlayClipAtPoint(chessmove, transform.position);
            Chessmans[selectedChessman.CurrentX, selectedChessman.CurrentY] = null;
            selectedChessman.transform.position = TileCenter(x, y);
            selectedChessman.SetPosition(x, y);
            Chessmans[x, y] = selectedChessman;

            isWhiteTurn = !isWhiteTurn;
            Debug.Log("Black Turn");
            turnindicator.text = "Black Player's Turn";
        }
        if (isWhiteTurn)
        {
            turnindicator.text = "White Player's Turn";
        }
        Highlights.Instance.Hidehighlights();
        selectedChessman = null;
    }

    private void UpdateSelection()
    {
        if (!Camera.main)
            return;

        RaycastHit hit;
        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition),out hit,25.0f,LayerMask.GetMask("ChessPlane")))
        {
            selectionX = (int)hit.point.x;
            selectionY = (int)hit.point.z;
        
        }
        else
        {
            selectionX = -1;
            selectionY = -1;
        } 
    }

    private void SpawnChesspiece(int index,int x,int y)
    {
        GameObject go = Instantiate(chesspieceprefabs[index],TileCenter(x,y), Quaternion.Euler(-90,0,0)) as GameObject;
        go.transform.SetParent(transform);
        Chessmans[x, y] = go.GetComponent<Chessmans> ();
        Chessmans[x, y].SetPosition(x, y);
        activeChesspiece.Add(go);
    }

    private void SpawnAllChesspieces()
    {
        activeChesspiece = new List<GameObject>();
        Chessmans = new Chessmans[8, 8];
        
        // White Pieces

        //King
        SpawnChesspiece (0,3,0);

        //Queen
        SpawnChesspiece(1,4, 0);

        //Bishop
        SpawnChesspiece(2,5,0);
        SpawnChesspiece(2,2,0);

        //Knight
        SpawnChesspiece(3,6, 0);
        SpawnChesspiece(3,1, 0);

        //Rook
        SpawnChesspiece(4, 7, 0);
        SpawnChesspiece(4, 0, 0);

        //Pawn
        for (int i = 0; i < 8; i++)
            SpawnChesspiece(5,i, 1);

        //Black Pieces

        //King
        SpawnChesspiece(6,4, 7);

        //Queen
        SpawnChesspiece(7,3, 7);

        //Bishop
        SpawnChesspiece(8, 5, 7);
        SpawnChesspiece(8,2, 7);

        //Knight
        SpawnChesspiece(9,6, 7);
        SpawnChesspiece(9,1, 7);

        //Rook
        SpawnChesspiece(10,7, 7);
        SpawnChesspiece(10,0, 7);

        //Pawn
        for (int i = 0; i < 8; i++)
            SpawnChesspiece(11,i, 6);


    }

    private void SpawnRandomChesspieces()
    {
        activeChesspiece = new List<GameObject>();
        Chessmans = new Chessmans[8, 8];

        // White Pieces

        //King
        SpawnChesspiece(0, 3, 0);

        //Queen
        SpawnChesspiece(UnityEngine.Random.Range(1,4), 4, 0);

        //Bishop
        SpawnChesspiece(UnityEngine.Random.Range(1, 4), 5, 0);
        SpawnChesspiece(UnityEngine.Random.Range(1, 4), 2, 0);

        //Knight
        SpawnChesspiece(UnityEngine.Random.Range(1, 4), 6, 0);
        SpawnChesspiece(UnityEngine.Random.Range(1, 4), 1, 0);

        //Rook
        SpawnChesspiece(UnityEngine.Random.Range(1, 4), 7, 0);
        SpawnChesspiece(UnityEngine.Random.Range(1, 4), 0, 0);

        //Pawn
        for (int i = 0; i < 8; i++)
            SpawnChesspiece(5, i, 1);

        //Black Pieces

        //King
        SpawnChesspiece(6, 4, 7);

        //Queen
        SpawnChesspiece(UnityEngine.Random.Range(7, 10), 3, 7);

        //Bishop
        SpawnChesspiece(UnityEngine.Random.Range(7, 10), 5, 7);
        SpawnChesspiece(UnityEngine.Random.Range(7, 10), 2, 7);

        //Knight
        SpawnChesspiece(UnityEngine.Random.Range(7, 10), 6, 7);
        SpawnChesspiece(UnityEngine.Random.Range(7, 10), 1, 7);

        //Rook
        SpawnChesspiece(UnityEngine.Random.Range(7, 10), 7, 7);
        SpawnChesspiece(UnityEngine.Random.Range(7, 10), 0, 7);

        //Pawn
        for (int i = 0; i < 8; i++)
            SpawnChesspiece(11, i, 6);


    }



    private Vector3 TileCenter (int x,int y)
    {
        Vector3 origin = Vector3.zero;
        origin.x += (Tile_Size * x) + Tile_Offset;
        origin.z += (Tile_Size * y) + Tile_Offset;
        return origin;

    }

    private void DrawChessboard()
    {
        Vector3 widthLine = Vector3.right * 8;
        Vector3 heightLine = Vector3.forward * 8;

        for (int i = 0; i <= 8; i++)
        {
            Vector3 start = Vector3.forward * i;
            Debug.DrawLine(start, start + widthLine);
            for (int j = 0; j <= 8; j++ )
            {
                start = Vector3.right * j;
                Debug.DrawLine(start, start + heightLine);
            }
        }

        if(selectionX >= 0 && selectionY >= 0)
        {
            Debug.DrawLine(Vector3.forward * selectionY + Vector3.right * selectionX, Vector3.forward * (selectionY + 1) + Vector3.right * (selectionX + 1));
            Debug.DrawLine(Vector3.forward * (selectionY +1 ) + Vector3.right * selectionX, Vector3.forward * selectionY + Vector3.right * (selectionX + 1));
        }
    }

    private void EndGame()
    {
        if (isWhiteTurn)
        {
            Debug.Log("White team wins");
            turnindicator.text = "White team wins";
        }
        else
        {
            Debug.Log("Black team wins");
            turnindicator.text = "Black team wins";
        }
        AudioSource.PlayClipAtPoint(gamefinish, transform.position);

        foreach (GameObject go in activeChesspiece)
        Destroy(go);
        StartCoroutine("wait");
        

        
    }
    IEnumerator wait()
    {
        yield return new WaitForSeconds(5.0f);
        SceneManager.LoadScene("Menu");
    }
}


