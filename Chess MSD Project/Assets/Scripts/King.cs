﻿using UnityEngine;
using System.Collections;

public class King : Chessmans {

    public override bool[,] PossibleMove()
    {
        bool[,] r = new bool[8, 8];


        Chessmans c;
        int i, j;

        // Top 
        i = CurrentX -1;
        j = CurrentY +1;
        if(CurrentY != 7)
        {
            for(int k = 0; k < 3; k++)
            {
                if (i >=0 || i < 8)
                {
                    c = Boardmanager.Instance.Chessmans[i, j];
                    if (c == null)
                        r[i, j] = true;

                    else if (isWhite != c.isWhite)
                        r[i, j] = true;
                }
                i++;
            }
        }

        //Bottom
        i = CurrentX - 1;
        j = CurrentY - 1;
        if (CurrentY != 0)
        {
            for (int k = 0; k < 3; k++)
            {
                if (i >= 0 || i < 8)
                {
                    c = Boardmanager.Instance.Chessmans[i, j];
                    if (c == null)
                        r[i, j] = true;

                    else if (isWhite != c.isWhite)
                        r[i, j] = true;
                }

                i++;
            }
        }

            //Middle to Left
            if (CurrentX != 0)
            {
                c = Boardmanager.Instance.Chessmans[CurrentX - 1, CurrentY];
                if (c == null)
                    r[CurrentX - 1, CurrentY] = true;

                else if (isWhite != c.isWhite)
                    r[CurrentX - 1, CurrentY] = true;
            }

        //Middle right
        if (CurrentX != 7)
        {
            c = Boardmanager.Instance.Chessmans[CurrentX - 1, CurrentY];
            if (c == null)
                r[CurrentX + 1, CurrentY] = true;

            else if (isWhite != c.isWhite)
                r[CurrentX + 1, CurrentY] = true;
        }


        return r;
    }

        // Use this for initialization
        void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
