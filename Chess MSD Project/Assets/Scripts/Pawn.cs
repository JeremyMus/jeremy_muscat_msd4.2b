﻿using UnityEngine;
using System.Collections;

public class Pawn : Chessmans {

    public override bool [,] PossibleMove ()
    {
        bool[,] r = new bool[8, 8];
        Chessmans c, c2;

        //Possible Moves for White Pawn
        if (isWhite)
        {
            //Diagonal Left
            if (CurrentX != 0 && CurrentY != 7)
            {
                c = Boardmanager.Instance.Chessmans[CurrentX - 1, CurrentY + 1];
                if (c != null && !c.isWhite)
                    r[CurrentX - 1, CurrentY + 1] = true;


            }
            //Diagonal Right
            if (CurrentX != 7 && CurrentY != 7)
            {
                c = Boardmanager.Instance.Chessmans[CurrentX + 1, CurrentY + 1];
                if (c != null && !c.isWhite)
                    r[CurrentX + 1, CurrentY + 1] = true;
            }
            //Straight Move
            if (CurrentY!= 7 )
            {
                c = Boardmanager.Instance.Chessmans[CurrentX, CurrentY + 1];
                if (c == null)
                    r[CurrentX, CurrentY + 1] = true;
            }
            //2 Spaces Straight on first move
            if (CurrentY == 1)
            {
                c = Boardmanager.Instance.Chessmans[CurrentX, CurrentY + 1];
                c2 = Boardmanager.Instance.Chessmans[CurrentX, CurrentY + 2];
                if (c == null && c2 == null)
                    r[CurrentX, CurrentY + 2] = true;
            }
        }
        // Possible Moves for Black Pawn
        else
        {
            //Diagonal Left
            if (CurrentX != 0 && CurrentY != 0)
            {
                c = Boardmanager.Instance.Chessmans[CurrentX - 1, CurrentY - 1];
                if (c != null && c.isWhite)
                    r[CurrentX - 1, CurrentY - 1] = true;
            }
            //Diagonal Right
            if (CurrentX != 7 && CurrentY != 0)
            {
                c = Boardmanager.Instance.Chessmans[CurrentX + 1, CurrentY - 1];
                if (c != null && c.isWhite)
                    r[CurrentX + 1, CurrentY - 1] = true;
            }
            //Straight Move
            if (CurrentY != 0)
            {
                c = Boardmanager.Instance.Chessmans[CurrentX  , CurrentY - 1];
                if (c == null)
                    r[CurrentX, CurrentY - 1] = true;
            }
            // 2 Spaces Straight on first move
            if (CurrentY == 6)
            {
                c = Boardmanager.Instance.Chessmans[CurrentX, CurrentY - 1];
                c2 = Boardmanager.Instance.Chessmans[CurrentX, CurrentY - 2];
                if (c == null && c2 == null)
                    r[CurrentX, CurrentY - 2] = true;
            }
        }
    
        return r;
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
