﻿using UnityEngine;
using System.Collections;

public class Bishop : Chessmans {

    public override bool[,] PossibleMove()
    {
        bool[,] r = new bool[8, 8];
       

        Chessmans c;
        int i, j;

        // Top to left
        i = CurrentX;
        j = CurrentY;

        while (true)
        {
            i--;
            j++;
            if (i < 0 || j >= 8)
                break;

            c = Boardmanager.Instance.Chessmans[i, j];
            if (c == null)
                r[i, j] = true;
            else
            {
                if (isWhite != c.isWhite)
                    r[i, j] = true;
                break;
            }

            
        }
        // Top to Right
        i = CurrentX;
        j = CurrentY;

        while (true)
        {
            i++;
            j++;
            if (i >= 8 || j >= 8)
                break;

            c = Boardmanager.Instance.Chessmans[i, j];
            if (c == null)
                r[i, j] = true;
            else
            {
                if (isWhite != c.isWhite)
                    r[i, j] = true;
                break;
            }


        }

        // Down to left
        i = CurrentX;
        j = CurrentY;

        while (true)
        {
            i--;
            j--;
            if (i < 0 || j < 0)
                break;

            c = Boardmanager.Instance.Chessmans[i, j];
            if (c == null)
                r[i, j] = true;
            else
            {
                if (isWhite != c.isWhite)
                    r[i, j] = true;
                break;
            }


        }

        // Down to right
        i = CurrentX;
        j = CurrentY;

        while (true)
        {
            i++;
            j--;
            if (i < 0 || j < 0)
                break;

            c = Boardmanager.Instance.Chessmans[i, j];
            if (c == null)
                r[i, j] = true;
            else
            {
                if (isWhite != c.isWhite)
                    r[i, j] = true;
                break;
            }


        }
        return r;
    }

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
